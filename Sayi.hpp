#ifndef SAYI_HPP
#define SAYI_HPP

#include <string>
#include "BagliListe.hpp"
using namespace std;

class Sayi {
private:
	string sayi, temp, temp1, number;
	int gecici = 0;
	BagliListe *list;
public:
	int charToInt(char);
	void listeyeEkle(string);
	int islem(string);
	string carp(Sayi*);
	string carp();
	int getUzunluk();
	string getSayi();
	Sayi();
	Sayi(string);
};

#endif
