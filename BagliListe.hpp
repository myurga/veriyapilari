#ifndef BAGLILISTE_HPP
#define BAGLILISTE_HPP

#include <iostream>
#include "Dugum.hpp"
using namespace std;

class BagliListe {
private:
	Dugum *basDugum, *gezici, *tmp; // i�inde eleman olmayan sadece listenin ba��n� tutan d���m.

public:
	BagliListe() {
		basDugum = new Dugum(); // Bo� bir d���m olu�turuluyor
		gezici = basDugum;
	}

	void Ekle(const int& yeni) {
		if(basDugum == NULL) {
			basDugum->sonraki = new Dugum(yeni); // basDugumun sonraki g�sterdi�i yere "new" ile D���m olu�turup i�ine de�er att�k.
		}
		else {
			while (gezici->sonraki != NULL) {
				gezici = gezici->sonraki; // Sonraki dugum NULL olana kadar ilerleyecek.
			}
			gezici->sonraki = new Dugum(yeni); // Sonraki dugum NULL oldugu anda WHILE dan ��kacak ve gezici->sonraki 'de yeni bir d���m olu�turacak
		}		
	}

	bool bosMu() {
		return basDugum == NULL;
	}

	void listele() {
		tmp = basDugum;
		while (tmp->sonraki != NULL) {
			cout << tmp->sonraki->veri << endl; // Sonraki dugum NULL olana kadar ilerleyecek.
			tmp = tmp->sonraki; 
		}
	}

	~BagliListe() {
	}
};

#endif
