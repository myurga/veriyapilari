#ifndef DUGUM_HPP
#define DUGUM_HPP

#include <iostream>

class Dugum {
public:
	int veri;
	Dugum *sonraki;

	Dugum() { // Dugum oluşturulduğunda otomatik olarak sonraki = NULL olacak.
		this->sonraki = NULL;
		//this->onceki = NULL;
	}

	Dugum(int veri, Dugum *sonr = NULL) {
		this->veri = veri;
		this->sonraki = sonr;
	}
};

#endif
